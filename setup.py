from distutils.core import setup

setup(
    name='anderson.django-custom-user',
    version='0.2',
    packages=['custom_auth'],
    url='https://bitbucket.org/jochangmin/django-custom-user',
    license='MIT',
    author='Anderson Jo',
    author_email='a141890@gmail.com',
    description=''
)
